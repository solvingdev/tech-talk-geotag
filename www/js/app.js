/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested starting place for your code.
// It is completely optional and not required.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false app:false, dev:false, cordova:false */


// For improved debugging and maintenance of your app, it is highly
// recommended that you separate your JavaScript from your HTML files.
// Use the addEventListener() method to associate events with DOM elements.
// For example:

// var el ;
// el = document.getElementById("id_myButton") ;
// el.addEventListener("click", myEventHandler, false) ;



// The function below is an example of the best place to "start" your app.
// It calls the standard Cordova "hide splashscreen" function. You can add
// other code to it or add additional functions that are triggered by the same
// event. The app.Ready event used here is created by the init-dev.js file.
// It serves as a unifier for a variety of "ready" events. See the init-dev.js
// file for more details. If you prefer the Cordova deviceready event, you can
// use that in addition to, or instead of this event.

// NOTE: change "dev.LOG" in "init-dev.js" to "true" to enable some console.log
// messages that can help you debug Cordova app initialization issues.

function onAppReady() {
    if( navigator.splashscreen && navigator.splashscreen.hide ) {   // Cordova API detected
        navigator.splashscreen.hide() ;
    }
}
document.addEventListener("app.Ready", onAppReady, false) ;


angular.module('geotag.controllers', ['ionic']);
angular.module('geotag.services', ['ngResource']);

angular.module('geotag', ['ionic', 'geotag.controllers', 'geotag.services'])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('home', {
      url: "/",
      templateUrl: "partials/home.html",
      controller: "HomeController as ctrl",
      resolve: {
          tags: function(Tag) {
              return Tag.query();
          }
      }
    })
    .state('detail', {
      url: "/detail/:id",
      templateUrl: "partials/detail.html",
      controller: "DetailController as ctrl",
      resolve: {
          tag: function(Tag, $stateParams) {
              var tag = new Tag({_id: $stateParams.id});
              return tag.$get();
          }
      }
    })
  
  $urlRouterProvider.otherwise("/");

});