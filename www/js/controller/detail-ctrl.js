angular.module('geotag.controllers')
.controller("DetailController", function(tag) {
    var self = this;
    
    self.tag = tag;
  
    canvas = new fabric.StaticCanvas('c');
      fabric.Image.fromURL(self.tag.photo_url, function(oImg) {
        
        var w = document.getElementById('canvas-wrp').clientWidth
        canvas.setWidth(w);
        
        oImg.scale(canvas.width / oImg.width);
        
        var h = oImg.getHeight();
        canvas.setHeight(h);
        
        
        canvas.add(oImg);
        
        var defaultShadow = { color: 'rgba(0,0,0,0.6)',
                                offsetX: 1,
                                offsetY: 1 };
                
        var mask = new fabric.Rect({
          left: w/2,
          top: h/2,
          width: w,
          height: h,
          originX: 'center',
          originY: 'center'
        });

        mask.setGradient('fill', {
          x1: 0,
          y1: mask.height,
          x2: 0,
          y2: 0,
          colorStops: {
            0: 'rgba(0, 0, 0, 0.4)',
            1: 'rgba(255, 255, 255, 0.0)'
          }
        });
        
        canvas.add(mask);
        
        
        var textLocality = new fabric.Text(self.tag.locality, {
              fontFamily: 'AvenirNext-Medium',
              fontSize: 30,
              //stroke: '#777',
              fill: '#FFF',
              //strokeWidth: 1,
              originX: 'right',
              originY: 'bottom',
              textAlign: 'right',
              left: w-15,
              top: h*0.9
        });
        
        textLocality.setShadow(defaultShadow);
        canvas.add(textLocality);
        
        var textCoords = new fabric.Text(self.tag.coords.lat  + ", " + self.tag.coords.long, {
              fontFamily: 'AvenirNext-Medium',
              fontSize: 15,
              //stroke: '#777',
              fill: '#FFF',
              //strokeWidth: 1,
              originX: 'right',
              originY: 'top',
              textAlign: 'right',
              left: w-15,
              top: h*0.9
        });
        
        textCoords.setShadow(defaultShadow);
        canvas.add(textCoords);
    });
})