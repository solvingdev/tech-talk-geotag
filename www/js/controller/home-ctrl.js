angular.module('geotag.controllers')
.controller("HomeController", function(tags, Tag, $scope, $state, $window, $ionicLoading){
  var self = this;

  self.listCanSwipe = true;

  self.share = function(tag) {
      console.log("Sharing tag " + tag._id)
  };

  self.delete = function(tag) {
      console.log("Deleting tag " + tag._id)
      
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner> Loading..."
      });
      
      tag.$delete({}, function() {
        $ionicLoading.hide();
        self.tags.splice(self.tags.indexOf(tag), 1);
        $scope.$apply();
      })
  };

  self.doRefresh = function() {
    
    var result = Tag.query({}, function() {
      self.tags = result;
      $scope.$broadcast('scroll.refreshComplete');
    })
  };

  self.takePicture = function() {
    
    var cameraOptions = {
      quality : 75,
      destinationType : Camera.DestinationType.DATA_URL,
      sourceType : Camera.PictureSourceType.CAMERA,
      allowEdit : true,
      encodingType: Camera.EncodingType.PNG,
      correctOrientation: true,
      targetWidth: 800,
      targetHeight: 800,
      saveToPhotoAlbum: false
    };
    
    navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
    
    function cameraSuccess(imageData) {
      
      if ($window.plugins && $window.plugins.toast) {
        $window.plugins.toast.showShortTop('Immagine acquisita',
                                         function(a){console.log('toast success: ' + a)},
                                         function(b){alert('toast error: ' + b)}
                                        );
      }
      
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner> Loading..."
      });
      
      navigator.geolocation.getCurrentPosition(geolocationSuccess,
                                               geolocationError,
                                               { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true });
      
      function geolocationSuccess(position) {
        
        var tag = new Tag({
          coords: {
            lat: position.coords.latitude,
            long: position.coords.longitude
          },
          filename: new Date().getTime() + "_pic.png",
          mimetype: "image/png",
          photo: imageData
        });
        
        tag.$save({}, function() {
            $ionicLoading.hide();
            self.tags.push(tag);
            $state.go('detail', {id: tag._id});
          },
          function() {
            $ionicLoading.hide();
        });
        
      }
      
      function geolocationError(err) {
        $ionicLoading.hide();
        console.log("Errore durante la geolocalizzazione", err);
        
        if ($window.plugins && $window.plugins.toast) {
          $window.plugins.toast.showShortTop("Errore durante la geolocalizzazione",
                                           function(a){console.log('toast success: ' + a)},
                                           function(b){alert('toast error: ' + b)}
                                          );
        }
      }

    }
    
    function cameraError(message) {
      if ($window.plugins && $window.plugins.toast) {
        $window.plugins.toast.showShortTop("Errore acquisizione immagine: " + message,
                                         function(a){console.log('toast success: ' + a)},
                                         function(b){alert('toast error: ' + b)}
                                        );
      }
    }
  }

  self.tags = tags;
})