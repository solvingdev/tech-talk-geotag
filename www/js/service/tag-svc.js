angular.module('geotag.services')
.factory('Tag', function($resource) {    
    return $resource("https://geotag-be.herokuapp.com/geotag",
                     {_id: '@_id'},
                     {query: {method: 'GET', url: 'https://geotag-be.herokuapp.com/geotags', isArray: true}}
                    )
})